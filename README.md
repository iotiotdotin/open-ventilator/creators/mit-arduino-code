# README

This is a sheild compatible with the Arduino Mega 2560

Contains
-1x DRV8825 stepper motor driver with different connection headers
-LCD screen connection with Contrast Potentiometer
-Buzzer Circuit with Volume Potentiometer
-LED and button connections
-Extra Power Out connections

2D
[](VentiSheild_PCB/2D_screenshot.png)

3D
[](VentiSheild_PCB/3D_screenshot.png)


